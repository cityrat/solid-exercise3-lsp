public class Manager extends Employee implements ManagerMethods, EmployeeMethods {
    double hourlyPayRate = 19.75;

    @Override
    public void calculateGrossPay(double hoursWorked){
        if(hoursWorked > BASE_HOURS){
            grossPay = (hourlyPayRate * BASE_HOURS) + ((hoursWorked - BASE_HOURS) * hourlyPayRate * OVERTIME_RATE);
        }
        else
        {
            grossPay = hoursWorked * hourlyPayRate;
        }
    }

    public void generatePerformanceReview(Managed emp){
        // Simulate reviewing a direct report
        System.out.println("I'm reviewing a direct report's performance.");
    }
}
