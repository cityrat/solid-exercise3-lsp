public class Main {

    public static void main(String[] args) {

        Manager accountingVP = new Manager();

        accountingVP.setFirstName("Emma");
        accountingVP.setLastName("Stone");
        accountingVP.calculateGrossPay(48);

        System.out.println(accountingVP.getFirstName() + "'s gross pay is " + Util.round(accountingVP.grossPay, 2));

        Employee emp = new Employee();

        emp.setFirstName("Tim");
        emp.setLastName("Corey");
        emp.assignManager(accountingVP, accountingVP.getFirstName(), accountingVP.getLastName());
        emp.calculateGrossPay(25);

        System.out.println(emp.getFirstName() + "'s gross pay is " + Util.round(emp.grossPay, 2));


        CEO boss = new CEO();
        boss.setFirstName("Big");
        boss.setLastName("Boss");
        boss.calculateGrossPay(40);
        boss.setCompanyPolicy();

        System.out.println(boss.getFirstName() + "'s gross pay is " + Util.round(boss.grossPay, 2));

//        emp.assignManager(accountingVP);
    }
}
