public class Employee implements EmployeeMethods, Managed {

    protected String firstName;
    protected String lastName;
    protected double grossPay;
    protected Employee manager = null;
    protected final double BASE_HOURS = 40;
    protected final double OVERTIME_RATE = 1.5;
    double hourlyPayRate = 12.50;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getGrossPay() {
        return grossPay;
    }

    public void setGrossPay(double grossPay) {
        this.grossPay = grossPay;
    }

    public Employee getManager() { return manager; }

    public void assignManager(ManagerMethods manager, String first, String last) {
        System.out.println(this.getFirstName() +" " +  this.getLastName() + " is now being managed by " + first + " " + last);
    }

    public void calculateGrossPay(double hoursWorked){


        if(hoursWorked > BASE_HOURS){
            grossPay = (hourlyPayRate * BASE_HOURS) + ((hoursWorked - BASE_HOURS) * hourlyPayRate * OVERTIME_RATE);
        }
        else
        {
            grossPay = hoursWorked * hourlyPayRate;
        }
    }
}
