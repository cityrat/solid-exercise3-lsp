public class CEO extends Employee implements EmployeeMethods {

    public void setCompanyPolicy() {
        System.out.println(this.firstName + " " + this.lastName+ " sets policy");
    }
    @Override
    public void calculateGrossPay(double hoursWorked){
        double hourlyPayRate = 150;

        if(hoursWorked > BASE_HOURS){
            grossPay = (hourlyPayRate * BASE_HOURS) + ((hoursWorked - BASE_HOURS) * hourlyPayRate * OVERTIME_RATE);
        }
        else
        {
            grossPay = hoursWorked * hourlyPayRate;
        }
    }

//    @Override
//    public void assignManager(Employee manager){
//        System.out.println("The CEO has no manager.");
//        System.exit(0);
//    }

//    public void generatePerformanceReview(){
//        // Simulate reviewing a direct report
//        System.out.println("I'm reviewing a direct report's performance.");
//    }
}
